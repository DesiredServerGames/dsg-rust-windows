﻿Configuration Push-RustServer
{
    
[cmdletbinding()]
param(
    [Parameter(Mandatory=$true)][string[]]$serverName = '',
    [Parameter(Mandatory=$true)][string[]]$serverIdentity = '',
    [Parameter(Mandatory=$true)][int[]]$serverMaxPlayers = '',
    [Parameter(Mandatory=$true)][string[]]$serverRconPassword = '',
    [Parameter(Mandatory=$true)][ValidateNotNullOrEmpty()][ValidateSet($false,$true)][bool[]]$OxideMod,
    [Parameter(Mandatory=$true)][ValidateRange(10,30)]$servertickrate = '10',
    [Parameter(Mandatory=$false)][ValidateSet("Procedural Map","Barren","HapisIsland","SavasIsland","SavasIsland_koth")][string[]]$serverlevel = 'Procedural Map',
    [Parameter(Mandatory=$false)][string[]]$serverdescription = '',
    [Parameter(Mandatory=$false)][string[]]$serverurl = '',
    [Parameter(Mandatory=$false)][string[]]$serverheaderimage = '',
    [Parameter(Mandatory=$false)][ValidateRange(1000,6000)][int[]]$serverWorldSize = '3000',
    [Parameter(Mandatory=$false)][int[]]$serverSaveInterval = '600',
    [Parameter(Mandatory=$false)][int[]]$serverSeed = '12345'
    )


Import-DscResource -ModuleName 'PSDesiredStateConfiguration', 'FileDownloadDSC', 'cFirewall', 'FileContentDsc', '7ZipArchiveDsc'

    Node localhost
    {
        File SteamCMD {
            Type            = 'Directory'
            DestinationPath = 'c:\dsgames\dsg-server\rust'
            Ensure          = "Present"
        }
        Script InstallRustServer{           
            SetScript = {
                Start-Process -FilePath "c:\dsgames\dsg-tools\steamcmd\Steamcmd.exe" -ArgumentList "+login anonymous +force_install_dir ""c:\dsgames\dsg-server\rust"" +app_update 258550 validate +exit" -Wait -PassThru -NoNewWindow;
            }
            TestScript = { 
                if(Test-Path "c:\dsgames\dsg-server\rust\RustDedicated.exe"){
                    $true
                }
                else {
                    $false
                }
            }
            GetScript = { 
                @{ Result = (Test-Path "‪c:\dsgames\dsg-server\rust\RustDedicated.exe") } 
            }
        }
        if($OxideMod){
            $tag = (invoke-WebRequest -Uri 'https://api.github.com/repos/OxideMod/Oxide.Rust/releases/latest'  | ConvertFrom-Json)[0].tag_name
            
            FileDownload DownloadOxideMod {
                FileName = "C:\dsgames\dsg-server\rust\Oxide.Rust-linux.zip"
                Url      = "https://github.com/OxideMod/Oxide.Rust/releases/download/$tag/Oxide.Rust-linux.zip"
            }
            x7ZipArchive UnzipOxideMod {
                Path        = "C:\dsgames\dsg-server\rust\Oxide.Rust-linux.zip"
                Destination = "C:\dsgames\dsg-server\rust"
                clean       = $True
                DependsOn   = "[FileDownload]DownloadOxideMod"
            }
        }
        Else{}
        cFirewallRule Rust-TCP {
            Name        = 'Rust-TCP'
            Description = 'Rust-server (TCP-in)'
            Action      = 'Allow'
            Direction   = 'Inbound'
            LocalPort   = ('28015-28016')
            Protocol    = 'TCP'
            Profile     = 'All'
            Enabled     = $True
            Ensure      = 'Present'
        }
        cFirewallRule Rust-UDP {
            Name        = 'Rust-UDP'
            Description = 'Rust-server (UDP-in)'
            Action      = 'Allow'
            Direction   = 'Inbound'
            LocalPort   = ('28015-28016')
            Protocol    = 'UDP'
            Profile     = 'All'
            Enabled     = $True
            Ensure      = 'Present'        
        }
        [string]$DSCRessource = $(get-item $PSScriptRoot) -replace "$((get-item $PSScriptRoot).Name)",""
        File CopyScriptStart {
            SourcePath      = "$DSCRessource\DSCRessource\start_headless_server.bat"
            DestinationPath = 'C:\dsgames\dsg-server\rust\start_headless_server.bat'
            Type            = "file"
            Ensure          = "present"
        }
        ReplaceText NameServer {
            Path      = 'c:\dsgames\dsg-server\rust\start_headless_server.bat'
            Search    = 'serverName'
            Type      = 'Text'
            Text      = "`"$serverName`""
            DependsOn = "[File]CopyScriptStart"
        }
        ReplaceText serverPort {
            Path      = 'c:\dsgames\dsg-server\rust\start_headless_server.bat'
            Search    = 'serverPort'
            Type      = 'Text'
            Text      = "28015"
            DependsOn = "[File]CopyScriptStart"
        }        
        ReplaceText serverIdentity {
            Path      = 'c:\dsgames\dsg-server\rust\start_headless_server.bat'
            Search    = 'serverIdentity'
            Type      = 'Text'
            Text      = "$serverIdentity"
            DependsOn = "[File]CopyScriptStart"
        }
        ReplaceText serverSeed {
            Path      = 'c:\dsgames\dsg-server\rust\start_headless_server.bat'
            Search    = 'serverSeed'
            Type      = 'Text'
            Text      = "$serverSeed"
            DependsOn = "[File]CopyScriptStart"
        }
        ReplaceText serverMaxPlayers {
            Path      = 'c:\dsgames\dsg-server\rust\start_headless_server.bat'
            Search    = 'serverMaxPlayers'
            Type      = 'Text'
            Text      = "$serverMaxPlayers"
            DependsOn = "[File]CopyScriptStart"
        }
        ReplaceText serverWorldSize {
            Path      = 'c:\dsgames\dsg-server\rust\start_headless_server.bat'
            Search    = 'serverWorldSize'
            Type      = 'Text'
            Text      = "$serverWorldSize"
            DependsOn = "[File]CopyScriptStart"
        }
        ReplaceText serverSaveInterval {
            Path      = 'c:\dsgames\dsg-server\rust\start_headless_server.bat'
            Search    = 'serverSaveInterval'
            Type      = 'Text'
            Text      = "$serverSaveInterval"
            DependsOn = "[File]CopyScriptStart"
        }
        ReplaceText serverRconPort {
            Path      = 'c:\dsgames\dsg-server\rust\start_headless_server.bat'
            Search    = 'serverRconPort'
            Type      = 'Text'
            Text      = "28016"
            DependsOn = "[File]CopyScriptStart"
        }
        ReplaceText serverRconPassword {
            Path      = 'c:\dsgames\dsg-server\rust\start_headless_server.bat'
            Search    = 'serverRconPassword'
            Type      = 'Text'
            Text      = "$serverRconPassword"
            DependsOn = "[File]CopyScriptStart"
        }
        ReplaceText serverlevel {
            Path      = 'c:\dsgames\dsg-server\rust\start_headless_server.bat'
            Search    = 'serverlevel'
            Type      = 'Text'
            Text      = "$serverlevel"
            DependsOn = "[File]CopyScriptStart"
        } 
        ReplaceText serverdescription {
            Path      = 'c:\dsgames\dsg-server\rust\start_headless_server.bat'
            Search    = 'serverdescription'
            Type      = 'Text'
            Text      = "$serverdescription"
            DependsOn = "[File]CopyScriptStart"
        }
        ReplaceText serverurl {
            Path      = 'c:\dsgames\dsg-server\rust\start_headless_server.bat'
            Search    = 'serverurl'
            Type      = 'Text'
            Text      = "`"$serverurl`""
            DependsOn = "[File]CopyScriptStart"
        } 
        ReplaceText serverheaderimage {
            Path      = 'c:\dsgames\dsg-server\rust\start_headless_server.bat'
            Search    = 'serverheaderimage'
            Type      = 'Text'
            Text      = "`"$serverheaderimage`""
            DependsOn = "[File]CopyScriptStart"
        }                                                                            
    }
}    
Push-RustServer -OutputPath $PSScriptRoot `
                -servername 'mon serveur test' `
                -serverIdentity 'tintin' `
                -serverMaxPlayers '10' `
                -serverRconPassword 'superpassword' `
                -OxideMod $false
                