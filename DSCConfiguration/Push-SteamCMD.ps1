Configuration Push-SteamCMD
{
param()


Import-DscResource -ModuleName 'PSDesiredStateConfiguration', 'FileDownloadDSC'

    Node localhost
    {
         File SteamCMD {
            Type            = 'Directory'
            DestinationPath = 'C:\tmp'
            Ensure          = "Present"
         }
         FileDownload SteamCMD {
            FileName  = "c:\tmp\steamcmd.zip"
            Url       = "https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip"
            DependsOn = "[File]SteamCMD"
        }        
        Archive livraison {
			Ensure      = "Present"
			Path        = "c:\tmp\steamcmd.zip"
			Destination = "c:\dsgames\dsg-tools\steamcmd\"
			Force       = $true
            DependsOn   = "[FileDownload]SteamCMD"
		}       
        WindowsProcess SteamCMD {
            path             = "c:\dsgames\dsg-tools\steamcmd\steamcmd.exe"
            Arguments        = "+login anonymous +exit"
            WorkingDirectory = "c:\dsgames\dsg-tools\steamcmd\"
            Ensure           = "present"
            DependsOn        = "[Archive]livraison"
        }
    }
}

Push-SteamCMD -OutputPath $PSScriptRoot