## Ansible playbook: Rust and Rust Oxide Mod

[README English version](./README.EN.md)

## Description
Installation, configure et sécurisation d'un serveur Rust avec ou sans Oxide Mod sur windows (WS 10-11/WS server 2012-2016-2019).

## Visuels
__WINDOWS 10__

_Youtube Bientôt disponible_

__WINDOWS SERVER 2016__

_Youtube Bientôt disponible_

## Préalable
Vérifier au préalable l'état d'autorisation d'éxecution de scripts, toutes les commandes doivent être lancer en mode ADMINISTRATEUR:

```powershell
PS C:\WINDOWS\system32> Get-ExecutionPolicy
RemoteSigned

PS C:\WINDOWS\system32> Set-ExecutionPolicy Unrestricted
"Modification de la stratégie d'exécution La stratégie d’exécution permet de vous prémunir contre les scripts que vous jugez non fiables. En modifiant la
stratégie d’exécution, vous vous exposez aux risques de sécurité décrits dans la rubrique d’aide about_Execution_Policies à l’adresse https://go.microsoft.com/fwlink/?LinkID=135170. Voulez-vous modifier la stratégie d’exécution ?
[O] Oui  [T] Oui pour tout  [N] Non  [U] Non pour tout  [S] Suspendre  [?] Aide (la valeur par défaut est « N ») :" T

PS C:\WINDOWS\system32> Get-ExecutionPolicy
Unrestricted
```

## Role Variables
Changer les valeurs selon votre convenance, les paramètres avec le mandatory à `$False` ne sont pas obligatoires car définit par une valeur par défaut.

Si vous voulez les modifiées, rajouter le paramètre à la dernière ligne de commande en surchargeant la valeur.

**DSCConfiguration\Push-RustServer.PS1**
```powershell
Configuration Push-RustServer
{
param(
	[Parameter(Mandatory=$true)][string[]]$serverName = '',
	[Parameter(Mandatory=$true)][string[]]$serverIdentity = '',
	[Parameter(Mandatory=$true)][int[]]$serverMaxPlayers = '',
	[Parameter(Mandatory=$true)][string[]]$serverRconPassword = '',
	[Parameter(Mandatory=$true)][ValidateNotNullOrEmpty()][ValidateSet($false,$true)][bool[]]$OxideMod,
	[Parameter(Mandatory=$true)][ValidateRange(10,30)]$servertickrate = '10',
	[Parameter(Mandatory=$false)][ValidateSet("Procedural Map","Barren","HapisIsland","SavasIsland","SavasIsland_koth")][string[]]$serverlevel = 'Procedural Map',
	[Parameter(Mandatory=$false)][string[]]$serverdescription = '',
	[Parameter(Mandatory=$false)][string[]]$serverurl = '',
	[Parameter(Mandatory=$false)][string[]]$serverheaderimage = '',
	[Parameter(Mandatory=$false)][ValidateRange(1000,6000)][int[]]$serverWorldSize = '3000',
	[Parameter(Mandatory=$false)][int[]]$serverSaveInterval = '600',
	[Parameter(Mandatory=$false)][int[]]$serverSeed = '12345'
...
...
}
Push-RustServer -OutputPath $PSScriptRoot `
                -servername 'mon serveur test' `
                -serverIdentity 'tintin' `
                -serverMaxPlayers '10' `
                -serverRconPassword 'superpassword' `
                -OxideMod $false  `
                -serverSaveInterval '300' `
                -serverWorldSize '4500'
```

## Dependances
Pas de dépendances


## Utilisation
EN MODE ADMINISTRATEUR
```powershell
PS C:\WINDOWS\system32> cd C:\DSG-Rust-Windows\
PS C:\DSG-Rust-Windows> .\Deploy-RustServer.PS1
VERBOSE: Preparation a l'installation des modules DSC
COMMENTAIRES : Acquiring providers for assembly:
....
```
## Support
Utiliser les issues prévu à cette effet ou le [discord](https://discord.gg/pt6DXQxvZs 'DISCORD - Desired Games Server') pour de plus amples informations

## Roadmap
Pour l'instant rien

## License
MIT

## Author Information
Ce rôle a été créé en 2021 par Didier MINOTTE.

